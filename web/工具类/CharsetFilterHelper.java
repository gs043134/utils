package com.linkage.vasplatform.util;

import java.io.UnsupportedEncodingException;
import java.util.Iterator;
import java.util.Set;

import com.linkage.appframework.data.IData;
/*
 * 版权：Copyright By Linkage Technology Co.,Ltd.
 * 描述：字符编码过滤助手类
 * 修改人：周立(3511)
 * 修改时间：2009-08-06
 */
public class CharsetFilterHelper {
	   public static IData filter(IData data) throws UnsupportedEncodingException{
		   Set keys=data.keySet();//获得所有键
		   Iterator<String>  iterator=keys.iterator();
		   while(iterator.hasNext()){
			   String key=iterator.next();
			   String value=new String(data.getString(key).getBytes("ISO-8859-1"),"gbk");
			   data.put(key, value);   
		   }
		 
		   return data;
	   }

}
