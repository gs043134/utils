package com.linkage.olcom.common;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import com.enterprisedt.net.ftp.FTPException;
import com.enterprisedt.net.ftp.FTPFile;
import com.enterprisedt.net.ftp.FileTransferClient;

public class FTP {

	private FileTransferClient ftp = null;

	public FTP(String host, String username, String password)
			throws FTPException {
		ftp = new FileTransferClient();
		ftp.setRemoteHost(host);
		ftp.setUserName(username);
		ftp.setPassword(password);
	}

	public void connect() throws FTPException, IOException {
		ftp.connect();
	}

	public void disconnect() throws FTPException, IOException {
		ftp.disconnect();
	}

	public void deleteRemoteFile(String remoteFile){
		try {
			ftp.deleteFile(remoteFile);
		} catch (FTPException e) {
			throw new RuntimeException(e);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	public void deleteRemoteDirectory(String remoteDir){
		try {
			FTPFile[] fs = ftp.directoryList(remoteDir);
			for(FTPFile f : fs){
				String subdir = remoteDir+"/"+f.getName();
				if(f.isDir()){
					deleteRemoteDirectory(subdir);
				}else{
					ftp.deleteFile(subdir);
				}
			}
			ftp.deleteDirectory(remoteDir);
		} catch (FTPException e) {
			throw new RuntimeException(e);
		} catch (IOException e) {
			throw new RuntimeException(e);
		} catch (ParseException e) {
			throw new RuntimeException(e);
		}
	}

	/*
	 * 上传文件夹
	 */
	public void uploadFolder(String local, String remoteDir, String folderName,
			boolean mkRemoteFolder) throws FTPException, IOException{
		if(mkRemoteFolder){
			this.uploadFolder(local, remoteDir, folderName);
		}else{
			uploadDirectory(local, remoteDir);
		}
	}

	/*
	 * 上传文件夹
	 */
	public void uploadFolder(String local, String remoteDir, String folderName) throws FTPException, IOException{
		ftp.createDirectory(remoteDir + "/" + folderName);
		uploadDirectory(local, remoteDir + "/" + folderName);
	}

	/*
	 * 上传文件或文件夹
	 */
	public void upload(String local, String remoteDir, String filename) throws FTPException, IOException{
		File dir = new File(local);
		if(dir.isDirectory()){
			ftp.createDirectory(remoteDir + "/" + dir.getName());
			uploadDirectory(local,remoteDir + "/" + dir.getName());
		}else{
			ftp.changeDirectory(remoteDir);
			ftp.uploadFile(local, filename);
		}
	}
	private void uploadDirectory(String localDir, String remoteDir) throws FTPException, IOException{
		ftp.changeDirectory(remoteDir);
		File dir = new File(localDir);
		File[] files = dir.listFiles();
		for(File f : files){
			if(f.isFile()){
				ftp.uploadFile(localDir+"/"+f.getName(), f.getName());
			}else{
				ftp.createDirectory(f.getName());
				uploadDirectory(localDir+"/"+f.getName(),remoteDir+"/"+f.getName());
			}
		}
		ftp.changeToParentDirectory();
	}

	public void fetch(String remotePath, String remoteFile,
			String localPath,String localFile)
	throws FTPException, IOException {
		ftp.changeDirectory(remotePath);
		File d = new File(localPath);
		if(!d.exists()){
			d.mkdirs();
		}
		ftp.downloadFile(localPath + localFile, remoteFile);
	}

	public List<String> getFileList(String remotePath)
	   throws FTPException, IOException, ParseException{
		List<String> fls = new ArrayList<String>();
		FTPFile[] ff =  ftp.directoryList(remotePath);
		for(FTPFile f : ff){
			fls.add(f.getName());
		}
		return fls;
	}

	public static void main(String[] args) throws FTPException, IOException, ParseException{
    	FTP t = new FTP("127.0.0.1", "test", "test123");
    	t.connect();
        List<String> s = t.getFileList("E:/temp/test1");
        System.out.println(s.size());
        t.uploadFolder("E:/temp/test1", "E:/temp/test", "agent");

//    	t.deleteRemoteFile("/home/zhouhl/mp/agent/Default.rdp");
//    	t.deleteRemoteDirectory("/home/zhouhl/mp/agent");
		t.disconnect();
	}

}
