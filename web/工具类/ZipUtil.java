package com.linkage.olcom.common;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * 业务工具类
 * 
 * @author 	wangt
 * @version 1.0 2009/2/20
 * @since	JDK1.5
 */
public class ZipUtil {

	/**
	 * 解压ZIP文件
//	 * @param file ZIP file
	 * @param dir 解压后保存目录
	 * @throws IOException 
	 * @throws Exception
	 */
	/*
	public static void unZip(File file, String dir) throws Exception {
		ZipFile zipFile = new ZipFile(file);
		Enumeration<?> e = zipFile.getEntries();
		ZipEntry ze = null;
		while (e.hasMoreElements()) {
			ze = (ZipEntry) e.nextElement();
			if (ze.isDirectory()) {
				File directory = new File(dir, ze.getName());
				if (!directory.exists()) {
					directory.mkdirs();
				}
			}
			// if file,unzip it
			if (!ze.isDirectory()) {
				File myFile = new File(dir, ze.getName());
				InputStream in = zipFile.getInputStream(ze);
				DataOutputStream dout = new DataOutputStream(
						new FileOutputStream(myFile));
				byte[] b = new byte[1024];
				int len = 0;
				while ((len = in.read(b)) != -1) {
					dout.write(b, 0, len);
				}
				dout.close();
			}
		}

	}*/
	
	
	public static void unStdZip(File zipFile, String dir) throws IOException{
		ZipInputStream zin = new ZipInputStream(new FileInputStream(zipFile));
		ZipEntry ze = null;
		while ((ze = zin.getNextEntry()) != null) {
			 if (ze.isDirectory()) {   
                 File directory = new File(dir, ze.getName());   
                 if (!directory.exists()) {   
                    directory.mkdirs();    
                 }   
                 zin.closeEntry();   
             }   
             // if file,unzip it   
             if (!ze.isDirectory()) {   
                 File myFile = new File(dir, ze.getName());   
                 FileOutputStream fout = new FileOutputStream(myFile);   
                 DataOutputStream dout = new DataOutputStream(fout);   
                 byte[] b = new byte[1024];   
                 int len = 0;   
                 while ((len = zin.read(b)) != -1) {   
                     dout.write(b, 0, len);   
                 }   
                 dout.close();   
                 fout.close();   
                 zin.closeEntry();   
             }
		}
		zin.close();
	}
	
	public static void main(String[] args){
		
		try {
			File f = new File("E:\\other\\test\\11\\agent.zip");
			unStdZip(f, "E:\\other\\test\\11\\1\\");
			
		} catch (Exception e) {
			
		}
	}

}
