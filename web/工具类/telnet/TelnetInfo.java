/*
 * @(#)TelnetInfor.java       1.0 07/11/07
 *
 * Copyright 2007 Linkage, Inc. All rights reserved.
 * Use is subject to license terms.
 */
package com.linkage.olcom.common.telnet;

import java.io.Serializable;

/**
 * This class implements Serializable
 * <p>
 * 
 * @author brucewang
 * @version 1.0 07/11/07
 * @see
 * @since
 */
public class TelnetInfo implements Serializable, Cloneable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3743771879325250710L;

	private String title;

	private String host;

	private int port = 23;

	private String userName;

	private String passWord;

	private String baseCommand;

	private String prompt = "$";

	private String endLine = "$";

	private int endFlag = 0;

	private String directory;

	private String charSet = "GBK";
	// 容量因子 （1024 * capacityFactor）
	private int capacityFactor = 10;

	public TelnetInfo() {
		super();
	}

	/**
	 * 
	 * @param host
	 * @param userName
	 * @param passWord
	 */
	public TelnetInfo(String host, String userName, String passWord) {
		super();
		this.host = host;
		this.userName = userName;
		this.passWord = passWord;
	}

	/**
	 * 
	 * @param host
	 * @param port
	 * @param userName
	 * @param passWord
	 */
	public TelnetInfo(String host, int port, String userName, String passWord) {
		super();
		this.host = host;
		this.port = port;
		this.userName = userName;
		this.passWord = passWord;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	public String toString() {
		return this.getClass().getName() + "[ title = " + this.title
				+ ", host = " + host + ", port = " + port + ", userName = "
				+ userName + ", passWord = " + passWord + ", baseCommand = "
				+ baseCommand + ", prompt = " + prompt + ", endLine = "
				+ endLine + ", directory = " + directory + ", charSet = "
				+ charSet + ", capacityFactor = " + capacityFactor + " ]";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#clone()
	 */
	public Object clone() {
		Object o = null;
		try {
			o = super.clone();
		} catch (CloneNotSupportedException e) {
		}
		return o;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	public int hashCode() {
		final int PRIME = 31;
		int result = 1;
		result = PRIME * result + ((host == null) ? 0 : host.hashCode());
		result = PRIME * result
				+ ((passWord == null) ? 0 : passWord.hashCode());
		result = PRIME * result + port;
		result = PRIME * result
				+ ((userName == null) ? 0 : userName.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;

		final TelnetInfo other = (TelnetInfo) obj;
		if (host == null) {
			if (other.host != null)
				return false;
		} else if (!host.equals(other.host))
			return false;
		if (passWord == null) {
			if (other.passWord != null)
				return false;
		} else if (!passWord.equals(other.passWord))
			return false;
		if (port != other.port)
			return false;
		if (userName == null) {
			if (other.userName != null)
				return false;
		} else if (!userName.equals(other.userName))
			return false;
		return true;
	}

	public String getCharSet() {
		return this.charSet;
	}

	public void setCharSet(String charSet) {
		this.charSet = charSet;
	}

	public String getBaseCommand() {
		return this.baseCommand;
	}

	public void setBaseCommand(String baseCommand) {
		this.baseCommand = baseCommand;
	}

	public String getPrompt() {
		return prompt.trim();
	}

	public void setPrompt(String prompt) {
		this.prompt = prompt;
	}

	public String getHost() {
		return this.host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public int getPort() {
		return this.port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public String getPassWord() {
		return this.passWord;
	}

	public void setPassWord(String passWord) {
		this.passWord = passWord;
	}

	public String getUserName() {
		return this.userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getDirectory() {
		return this.directory;
	}

	public void setDirectory(String directory) {
		this.directory = directory;
	}

	public int getCapacityFactor() {
		return this.capacityFactor;
	}

	public void setCapacityFactor(int capacityFactor) {
		this.capacityFactor = capacityFactor;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public int getEndFlag() {
		return endFlag;
	}

	public void setEndFlag(int endFlag) {
		this.endFlag = endFlag;
	}

	public String getEndLine() {
		return endLine;
	}

	public void setEndLine(String endLine) {
		this.endLine = endLine;
	}
}
