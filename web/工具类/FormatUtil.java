package com.linkage.olcom.common;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class FormatUtil {
	public static String formatCalender(Calendar cal, String format) {
		if (format == null || format.equals(""))
			format = "yyyy-MM-dd HH:mm:ss";
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		String dateStr = sdf.format(cal.getTime());
		return dateStr;

	}
	
	public static String formatDate(Date date, String format){
		return new SimpleDateFormat(format).format(date);
	}

	public static Calendar parseCalender(String time, String format) {
		if (format == null || format.equals(""))
			format = "yyyy-MM-dd HH:mm:ss";
		Calendar calendar = Calendar.getInstance();
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format);
		try {
			calendar.setTime(simpleDateFormat.parse(time));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return calendar;
	}

	/**
	 * 将20090308101423转换为2009-03-08 10:14:23
	 * @param time
	 * @return
	 */
    public static String changeLogDateTime(String time){	
		return time.subSequence(0, 4)+"-"+time.substring(4, 6)+
				"-"+time.substring(6, 8)+" "+time.substring(8, 10)
				+":"+time.substring(10, 12)+":"+time.substring(12);
		
	}

    /**
     * 返回开始时间和截止时间之间所差的时间间隔
     * @param beginTime
     * @param endTime
     * @return
     */
    public static String getMinusTime(String beginTime,String endTime){
    	SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");    
    	Date bdate = null;
    	Date edate = null;
		try {
			bdate = df.parse(beginTime);
		    edate=df.parse(endTime);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    	long value=edate.getTime()-bdate.getTime();
    	return getResultOfSecond(value);
    	
    }
    
    /**
     * 把秒数转换为天时分秒
     * @param v
     * @return
     */
    public static String getResultOfSecond(Long v){
    	v=v/1000;
    	String range = "";
		int s = Math.round(v % 60);
		range =s+"秒";
		Double sm = Math.floor(v / 60);
		if(sm > 0){
			int m = (int) Math.round(sm % 60); 
			range = m+"分" + range;
			Double sh = Math.floor(sm / 60);
			if(sh > 0){
				int h = (int)Math.round(sh % 24);
				range = h+"小时"+range;
				long sd = Math.round(sh / 24);
				if(sd > 0){
					range = sd+"天"+range;
				}
			}
		}	
		return range;
    	
    }
    
    /**
     * 获得某天是周几
     * 传入参数是String型
     * @param date
     * @return
     */
    public static String  getDayOfWeek(String date){
    	String result="";
    	Calendar c=parseCalender(date, "yyyyMMdd");
    	switch(c.get(Calendar.DAY_OF_WEEK)){
    	case 1 : result="周日"; break;
    	case 2 : result="周一"; break;
    	case 3 : result="周二"; break;
    	case 4 : result="周三"; break;
    	case 5 : result="周四"; break;
    	case 6 : result="周五"; break;
    	case 7 : result="周六"; break;
    	}
    	return result;   	
    }
    
    /**
     * 入参格式yyyyMMdd
     * @param time
     * @return
     */
    public static String getWeekNameByTime(String time){
    	String result="";
    	
    	String a=FormatUtil.formatCalender(CalendarUtil.getFirstDayOfWeek(), "yyyyMMdd");//本周第一天
    	String b=FormatUtil.formatCalender(CalendarUtil.getFirstDayOfLastWeek(), "yyyyMMdd");//上周第一天
    	String c=FormatUtil.formatCalender(CalendarUtil.getLastDayOfWeek(), "yyyyMMdd");//本周最后一天
    	
    	//不早于本周1也不晚于本周日的为本周
    	if((time.compareTo(a)>=0)&&(time.compareTo(c)<=0)){
    		result="本周";	
    	}
    	//早于本周1不早于上周1的为上周
    	else if((time.compareTo(a)<0)&&(time.compareTo(b)>=0)){
    		result="上周";
    	}
    	else result="不是本周也非上周";
    	return result;
    	
    }
    /**
     *  入参格式yyyyMMdd
     * @param time
     * @return
     */
    public static String getMonthNameByTime(String time){
    	String result="";
    	
    	String a=FormatUtil.formatCalender(CalendarUtil.getFirstDayOfMonth(), "yyyyMMdd");//本月第一天
    	String b=FormatUtil.formatCalender(CalendarUtil.getFirstDayOfLastMonth(), "yyyyMMdd");//上月第一天
    	String c=FormatUtil.formatCalender(CalendarUtil.getLastDayOfMonth(), "yyyyMMdd");//本月最后一天
    	
    	//晚于本月第一天，早于本月最后一天为本月
    	if((time.compareTo(a)>=0)&&(time.compareTo(c)<=0)){
    		result="本月";	
    	}
    	//早于本月第一天，不早于上月第一天
    	else if((time.compareTo(a)<0)&&(time.compareTo(b)>=0)){
    		result="上月";
    	}
    	else result="不是本月也非上月";
    	return result;
    	
    }
    /**
     *  入参格式yyyyMM
     * @param time
     * @return
     */
    public static String getYearName(String time){
    	String result="";
    	
    	String a=FormatUtil.formatCalender(CalendarUtil.getFirstMonthOfLastYear(),"yyyyMM");//去年第一个月
    	String b=FormatUtil.formatCalender(CalendarUtil.getLastMonthOfLastYear(), "yyyyMM");//去年最后一个月
    	String c=FormatUtil.formatCalender(CalendarUtil.getLastMonthOfThisYear(), "yyyyMM");//今年最后一个月
    	
    	//在去年第一个月和最后一个月之间的为去年
    	if((time.compareTo(a)>=0)&&(time.compareTo(b)<=0)){
    		result="去年";	
    	}
    	//去年最后一个月和今年最后一个月之间的为今年
    	else if((time.compareTo(b)>0)&&(time.compareTo(c)<=0)){
    		result="今年";
    	}
    	else result="不是今年也非去年";
    	return result;
    	
    }
    
	public static void print(Object obj) {
		System.out.println(obj.toString());
	}

	public static void main(String[] args) {
		// test formatCalender HHmmss
//		String DATE_FORMAT = "yyyyMMdd";
//		SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
//		Calendar c1 = Calendar.getInstance(); // today
//		System.out.println("Today is " + sdf.format(c1.getTime()));
//		print(formatCalender(new GregorianCalendar(), DATE_FORMAT));
//		System.out.println("2008-09-30 12:18:19".compareTo("2008-10-30 12:18:19"));
//		System.out.println(FormatUtil.getMinusTime("2008-09-30 12:18:19","2008-10-30 12:18:19"));
//		Date d1=new Date(86,8,4);
//		Date d2=new Date(86,10,28);
//		SimpleDateFormat sdf=new SimpleDateFormat("yyyyMMdd");
		//System.out.println(sdf.format(d1));
		//System.out.println(sdf.format(d2));
//		List<String> dates=CalendarUtil.getMonths("200801", "200904");
//		for(String s:dates){
//			System.out.println(s);
//		}
		
//		System.out.println(FormatUtil.getMonthNameByTime("20090420"));
//		System.out.println(FormatUtil.getMonthNameByTime("20090426"));
//		System.out.println(FormatUtil.getMonthNameByTime("20090427"));
//		System.out.println(FormatUtil.getMonthNameByTime("20090503"));
//		System.out.println(FormatUtil.getMonthNameByTime("20090316"));
//		System.out.println(FormatUtil.formatCalender(CalendarUtil.getLastMonthOfThisYear(),"yyyyMM"));
//		System.out.println(FormatUtil.getYearName("200901"));
//		System.out.println(FormatUtil.getYearName("200801"));
//		System.out.println(FormatUtil.getYearName("200812"));
//		System.out.println(FormatUtil.getYearName("200912"));
	}

}
