package com.linkage.olcom.common;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import com.linkage.olcom.domain.security.Tree;

/**
 * 业务工具类
 * 
 * @author 	zhouhl
 * @version 1.0 2008/12/26
 * @since	JDK1.5
 */
public class BusinessUtil {

	/**
	 * 树及树节点排重,排序<br>
	 * 当出现以下情况时，取其中一个，去掉一个（具体去掉哪个不确定）<br>
	 * 1.当集合中的某节点与集合中另一节点相同<br>
	 * 2.当集合中的某节点是集合中另一节点的子节点<br>
	 * 且同一个节点下的孩子节点按id排序
	 * 
	 * @param <T>
	 * @param collection 树（树节点）根节点集合
	 * @return 排重后的树集合
	 */
	@SuppressWarnings("unchecked")
	public static <T extends Tree<T>> List<T> reSchedule(Collection<T> collection){
		Object[] arr = collection.toArray();
		int n = arr.length;
		for(int i = 0; i < n-1; i++){
			Tree<T> oi = (Tree<T>)arr[i];
			if(oi == null){
				continue;
			}
			int k = i;
			for(int j = i+1; j < n; j++){
				Tree<T> oj = (Tree<T>)arr[j];
				if(oj != null){
					if(oi.equals(oj) || oi.hasDescendant(oj)){
						arr[j] = null;
					}else if(oj.hasDescendant(oi)){
						arr[i] = null;
						break;
					}else{
						Tree<T> ok = (Tree<T>)arr[k];
						if(ok.compareTo(oj) > 0){
							k = j;
						}
					}
				}
			}
			if(k != i && arr[i] != null){
				Object tmp = arr[k];
				arr[k] = arr[i];
				arr[i] = tmp;
			}
		}
		List<T> result = new ArrayList<T>();
		for(Object o : arr){
			if(o != null){
				result.add((T)o);
			}
		}
		return result;
	}
	
	//写文件
	public static void writeFile(File file, String text) throws IOException{
	  FileWriter fw = new FileWriter(file);
	  fw.write(text);
	  fw.flush();
	  fw.close();
	}
	
	/**
	 * 转换List为String
	 * 
	 * @param list
	 * @param token
	 */
	public static String listToString(List<String> list, String token){
		 StringBuilder sorts = new StringBuilder();
		 int len = list.size();
		 if(len == 0) return "";
		 for(int i = 0; i < len - 1; i++){
			 sorts.append(list.get(i)).append(token);
		 }
		 sorts.append(list.get(len - 1));
		 return sorts.toString();
	}
	
	/**
	 * 测试方法
	 * @param <K>
	 * @param <E>
	 * @param map
	 */
	public static <K,E> void printMap(Map<K,E> map){
		Iterator<K> iter = map.keySet().iterator();
		while(iter.hasNext()){
			K k = iter.next();
			E e = map.get(k);
			System.out.println(k.toString()+":"+e.toString());
		}
	}

	public static void createFileDir(String filePath) {
		File dir = new File(filePath);
		if(!dir.exists()){
			dir.mkdirs();
		}
	}

	public static String[] getNewArray(String[] oldArray, int len){
		if(oldArray.length >= len) return oldArray;
		String[] newArray = new String[len];
		for(int i = 0; i < oldArray.length; i++){
			newArray[i] = oldArray[i];
		}
		for(int i = oldArray.length; i < len; i++){
			newArray[i] = "";
		}
		return newArray;
	}
	
	public static List<String[]> getCtrlFileRecord(File file) 
	  throws IOException{
		BufferedReader br =new BufferedReader(new FileReader(file));
		String line = null;
		List<String[]> list = new ArrayList<String[]>();
	    while( (line = br.readLine() ) != null ){
	    	if(!line.trim().equals(""))
	    	  list.add(line.trim().split("\\s"));	
	    }
	    br.close();
		return list;
	}
	
	public static List<String[]> parserProperties(File file, String hostId) throws IOException{
		List<String[]> list = new ArrayList<String[]>();
		Properties props = new Properties();
		FileInputStream fis = new FileInputStream(file);
		props.load(fis);
		FileOutputStream fout = new FileOutputStream(file);
	
		Enumeration<?> e = props.propertyNames();
		while (e.hasMoreElements()) {
			String[] propArray = new String[2];
			String key = (String) e.nextElement();
			if (key != null && !key.trim().equals("")) {
				String value = props.getProperty(key);
				if(key.equals("ID")){
					propArray[1] = hostId;
					props.setProperty("ID", hostId);
				}else{
					propArray[1] = value;
				}
				propArray[0] = key;
				list.add(propArray);
			}
		}
		props.store(fout, "");
		fout.close();
		fis.close();
		return list;
	}
	
	
	public static void deleteDir(File path){   
		if(!path.exists()) return;   
		if(path.isFile()){   
			path.delete();   
			return;   
		}   
		File[] files = path.listFiles();       
		for(int i=0;i<files.length;i++){   
			deleteDir(files[i]);   
		}   
		path.delete();   
	}
	
	public static String strArrayToStr(String[] array, String token){
		StringBuilder sb = new StringBuilder();
		int len = array.length;
		for(int i = 0; i < len - 1; i++){
			sb.append(array[i]).append(token);
		}
		sb.append(array[len-1]);
		return sb.toString();
	}
	
}
