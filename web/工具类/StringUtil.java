package com.linkage.olcom.common;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import org.apache.commons.lang.CharUtils;
import org.apache.commons.lang.StringUtils;

/**
 * 字符串相关的工具方法
 * 
 * @author wuminzhe
 * 
 */
public class StringUtil {

	/**
	 * 获取第一行
	 * 
	 * @param msg
	 * @return
	 */
	public static String getFirstLine(String msg) {
		if (msg == null)
			return " ";
		String command = " ";
		if (StringUtils.contains(msg, "\n")) {
			int index = msg.indexOf("\n");

			command = msg.substring(0, index);
		} else {
			command = msg;
		}
		return command;
	}

	/**
	 * 获取最后一行
	 * 
	 * @param msg
	 * @return
	 */
	public static String getLastLine(String msg) {
		if (msg == null)
			return " ";
		String command = " ";
		if (StringUtils.contains(msg, "\n")) {
			int index = msg.lastIndexOf("\n");
			command = msg.substring(index + 1);
		} else {
			command = msg;
		}
		return command;
	}

	/*
	 * 删除第一行
	 */
	public static String deleteFirstLine(String msg) {
		if ((msg == null) || (msg == "")) {
			return "";
		}
		String command = "";
		if (StringUtils.contains(msg, "\n")) {
			int index = msg.indexOf("\n");

			command = msg.substring(index + 1);
		} else {
			command = msg;
		}
		return command;
	}

	/*
	 * 删除最后一行
	 */
	public static String deleteLastLine(String msg) {
		if ((msg == null) || (msg == "")) {
			return "";
		}
		String command = "";
		if (StringUtils.contains(msg, "\n")) {
			int index = msg.lastIndexOf("\n");
			command = msg.substring(0, index);
		} else {
			command = msg;
		}
		return command;
	}

	/*
	 * 从YYYYMMDDHH格式的日期中提取YYYY年MM月DD日
	 */
	public static String year2Day(String msg) {
		StringBuffer sb = new StringBuffer();

		if (msg.length() > 9) {
			return sb.append(msg.substring(0, 4)).append("年").append(
					msg.substring(4, 6)).append("月")
					.append(msg.substring(6, 8)).append("日").toString();
		} else {
			return msg;
		}
	}

	/*
	 * 把当前时间截断到天转化为YYYY-MM-DD格式输出、如2007-05-16
	 */
	public static String getCurrentDay() {
		Calendar calendar = Calendar.getInstance();

		return new StringBuffer().append(calendar.get(Calendar.YEAR)).append(
				"-").append(
				StringUtil.appendZero(calendar.get(Calendar.MONTH) + 1))
				.append("-").append(
						StringUtil.appendZero(calendar.get(Calendar.DATE)))
				.toString();
	}

	public static String getCurrentMonth(boolean append) {
		Calendar calendar = Calendar.getInstance();
		StringBuffer buffer = new StringBuffer();
		buffer.append(calendar.get(Calendar.YEAR));
		if (append) {
			buffer.append("-");
		}
		buffer.append(StringUtil.appendZero(calendar.get(Calendar.MONTH) + 1))
				.toString();
		return buffer.toString();
	}

	/*
	 * 把小于10的数字补零转为String,如 9 => 09
	 */
	public static String appendZero(int number) {
		if ((number <= 9) && (number >= 0)) {
			return "0" + number;
		}

		return "" + number;
	}

	/*
	 * 把数字组成的字符川转化为int "123" => 123
	 */
	public static int StringtoInt(String mesg) {
		if ((mesg == null) || mesg.equals("")) {
			return 0;
		}

		char[] chars = mesg.toCharArray();
		int length = chars.length;
		int m = 0;
		int j = length - 1;

		for (int i = 0; i < length; i++) {
			char ch = chars[i];

			if ((ch <= '9') && (ch >= '0')) {
				int d = new Double(Math.pow(10, j--)).intValue();

				int temp = CharUtils.toIntValue(ch) * d;

				m += temp;
			} else {
				return 0;
			}
		}

		return m;
	}

	/**
	 * 转化字符串为十六进制编码
	 * 
	 * @param s
	 * @return
	 */
	public static String toHexString(String s) {
		StringBuffer buffer = new StringBuffer();
		for (int i = 0; i < s.length(); i++) {
			buffer.append(Integer.toHexString((int) s.charAt(i)));
		}
		return buffer.toString();
	}

	/**
	 * 转化十六进制编码为字符串
	 * 
	 * @param s
	 * @return
	 */
	public static String toStringHex(String s) {
		byte[] baKeyword = new byte[s.length() / 2];
		for (int i = 0; i < baKeyword.length; i++) {
			try {
				baKeyword[i] = (byte) (0xff & Integer.parseInt(s.substring(
						i * 2, i * 2 + 2), 16));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		try {
			s = new String(baKeyword, "utf-8");// utf-8
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		return s;
	}

	/**
	 * 
	 * @param varList
	 * @return
	 */
	public static String[] splitVarList(String varList) {
		String[] vars = varList.split(StringUtil.toStringHex("01"));
		return vars;
	}

	public static String[] splitParam(String param, String spliter) {
		int beg = 0, end;
		List<String> list = new ArrayList<String>();
		while (true) {
			end = param.indexOf(spliter, beg);
			if (end == -1) {
				String str = param.substring(beg);
				list.add(str);
				break;
			}
			String str = param.substring(beg, end);
			list.add(str);
			beg = end + 1;
		}
		String[] a = new String[list.size()];
		list.toArray(a);
		return a;
	}

	public static void main(String[] args) {
		String s = "a,b,c";
		System.out.println(s.replace(",", "\\x01"));
		String line = "ab\nbc\ncd\n$ ";
		System.out.println("{" + StringUtil.getLastLine(line) + "}");
	}
}
