package com.linkage.component.util.file;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.fileupload.FileItem;
import com.linkage.appframework.data.IData;
import com.linkage.component.AppServlet;
import com.linkage.component.PageData;
import com.linkage.component.bean.adm.UtilBean;
import com.linkage.vasplatform.view.management.webservice.helper.ResourceBunderUtil;
import com.linkage.webframework.util.file.FileMan;

public class FileServlet extends AppServlet {

	public static final String FILE_ACTION_UPLOAD = "upload";
	public static final String FILE_ACTION_DOWNLOAD = "download";
	public static final String FILE_ACTION_DELETE = "delete";
	public static final String FILE_MODE_DOWN = "down";
	public static final String FILE_MODE_OPEN = "open";
	public static final String FILE_SOURCE_WEB = "web";
	public static final String FILE_SOURCE_DOMAIN = "domain";
	public static final String FTP_FILE_ACTION_DOWNLOAD = "ftpdownload";

	/**
	 * service down file(example1:attach?file_id=1 or
	 * attach?file_id=1&mode=open) down
	 * file(example2:attach?file_name=1.xls&file_path=/templet/a.xls or
	 * attach?file_name=1.xls&file_path=/templet/a.xls&mode=open)
	 * 
	 * @param request
	 * @param response
	 * @throws ServletException,IOException
	 */
	public void service(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		try {
			servletInit(request, response);

			PageData pd = getPageData(request);
			String action = pd.getParameter("action", FILE_ACTION_DOWNLOAD);
			
			UtilBean bean = new UtilBean();
			if (FILE_ACTION_UPLOAD.equals(action)) {
				FileItem item = pd.getFileItem("FILE_PATH");

				IData data = bean.createFile(pd, item,
						"", "", "");
				String file_id = (String) data.get("FILE_ID");
				String file_name = common.encodeCharset((String) data
						.get("FILE_NAME"));

				PrintWriter out = response.getWriter();
				out
						.println("<script language=\"JavaScript\" src=\"/component/scripts/public.js\"></script>");
				out
						.println("<script language=\"JavaScript\" src=\"/component/scripts/file.js\"></script>");
				out.println("<script>");
				out.println("    var retobj = new Object();");
				out.println("	 retobj.fileId = \"" + file_id + "\";");
				out.println("	 retobj.fileName = \"" + file_name + "\";");
				out.println("	 retobj.fileUrl = getFileImage(\"" + file_name
						+ "\") + \"<a href='/attach?file_id=" + file_id
						+ "' title='" + pd.getParameter("FILE_DESC", "") + "'>"
						+ file_name + "</a>\";");
				out.println("	 setReturnObj(retobj);");
				out.println("</script>");
			}
			if (FILE_ACTION_DOWNLOAD.equals(action)) {
				String mode = pd.getParameter("mode", FILE_MODE_DOWN);
				String file_id = pd.getParameter("file_id");
				String file_name = pd.getParameter("file_name");
				String file_path = pd.getParameter("file_path");
				String file_source = pd.getParameter("file_source",
						FILE_SOURCE_WEB);

				String full_name = null;
				if (file_path == null) {
					IData data = bean.queryFile(pd, file_id);
					if (data == null)
						common.error("文件记录 " + file_id + " 不存在!");

					full_name = (String) data.get("FILE_PATH") + "/"
							+ data.get("R_FILE_NAME");
					file_name = (String) data.get("FILE_NAME");
				} 
				else {
					full_name = (FILE_SOURCE_WEB.equals(file_source) ? request
							.getRealPath("/") : "")
							+ file_path;
					if (file_name == null)
						file_name = FileMan.getFileName(full_name);
				}

				if (FILE_MODE_DOWN.equals(mode)) {
					FileMan.downFile(response, full_name, file_name);
				}
				if (FILE_MODE_OPEN.equals(mode)) {
					FileMan.showFile(response, full_name, file_name);
				}
			}
			if(FTP_FILE_ACTION_DOWNLOAD.equals(action)){
				String filename=pd.getParameter("filename");
				if(filename!=null&&!"".equals(filename)){
				FileMan.downFile(response, ResourceBunderUtil.getProperties("localpath")+filename, filename);
				}
			}
			if (FILE_ACTION_DELETE.equals(action)) {
				String file_id = pd.getParameter("file_id");
				bean.deleteFile(pd, file_id);
			}
		} catch (Exception e) {
			servletReseted();
			// common.error(e);
			setErrorInfo(response, e);
		} finally {
			servletDetached();
		}
	}

}