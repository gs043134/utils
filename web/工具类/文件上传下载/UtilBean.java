package com.linkage.component.bean.adm;

import java.io.File;

import org.apache.commons.fileupload.FileItem;

import com.linkage.appframework.cache.EHCacheManager;
import com.linkage.appframework.cache.ICache;
import com.linkage.appframework.data.DataMap;
import com.linkage.appframework.data.IData;
import com.linkage.appframework.data.IDataset;
import com.linkage.component.AppBean;
import com.linkage.component.AppFactory;
import com.linkage.component.PageData;
import com.linkage.dbframework.util.Pagination;
import com.linkage.vasplatform.view.management.webservice.helper.ResourceBunderUtil;
import com.linkage.webframework.util.file.FileMan;

public class UtilBean extends AppBean {

	/**
	 * create file
	 * 
	 * @param pd
	 * @param data
	 * @throws Exception
	 */
	public void createFile(PageData pd, IData data) throws Exception {
		UtilDAO dao = new UtilDAO(pd, AppFactory.CENTER_CONNECTION_NAME);
		data.put("CREA_STAFF", pd.getContext().getStaffId());
		data.put("CREA_TIME", common.getSysTime());
		dao.insert("TD_M_FILE", data);
	}

	/**
	 * create file
	 * 
	 * @param pd
	 * @param item
	 * @param file_type
	 * @param file_kind
	 * @return IData
	 * @throws Exception
	 */
	public IData createFile(PageData pd, FileItem item, String file_type,
			String file_kind) throws Exception {
		return createFile(pd, item, file_type, file_kind, null);
	}

	/**
	 * create file
	 * 
	 * @param pd
	 * @param item
	 * @param file_type
	 * @param file_kind
	 * @param upload_path
	 * @return IData
	 * @throws Exception
	 */
	public IData createFile(PageData pd, FileItem item, String file_type,
			String file_kind, String upload_path) throws Exception {
		UtilDAO dao = new UtilDAO(pd, AppFactory.CENTER_CONNECTION_NAME);
		
		String FILE_PATH=ResourceBunderUtil.getProperties("upload_path");
		
		String file_id = dao.getSequence("SQL_FILE_ID");
		String file_name = FileMan.getFileName(item.getName());
		
	//	String type = getFileType(item.getContentType());
		
		
		String type =file_name.substring(file_name.lastIndexOf('.')+1);
			
			
		String FILENAME=file_id+"."+type;
		
		String file_size = String.valueOf(item.getSize());
		/**
		 * 可能会出现同名文件，最后用file_id最为新文件名， 这样不会重复，但是需要增加后缀
		 */
		FileMan.uploadFile(item, FILE_PATH, file_id,type);

		IData data = new DataMap();
		data.put("FILE_ID", file_id);
		data.put("R_FILE_NAME", FILENAME);//远程文件名
		data.put("FILE_KIND", type);
		data.put("FILE_NAME", file_name);
		data.put("FILE_PATH", FILE_PATH);
		data.put("FILE_SIZE", file_size);
		createFile(pd, data);

		return data;
	}

	/**
	 * 获得文件类型
	 * 
	 * @param contentType
	 * @return
	 */
	public String getFileType(String contentType){
		String filetype="mms";
    	if(contentType.equals(FileMan.CONTENT_TYPE_EXCEL)){
    		filetype=FileMan.FILE_TYPE_XLS;
    	}
    	else if(contentType.equals(FileMan.CONTENT_TYPE_IMAGE_GIF)){
    		filetype=FileMan.FILE_TYPE_GIF;
    	}
		else if(contentType.equals(FileMan.CONTENT_TYPE_IMAGE_JPEG)){
			filetype=FileMan.FILE_TYPE_JPEG;
		}
		else if(contentType.equals(FileMan.CONTENT_TYPE_IMAGE_PNG)){
			filetype=FileMan.FILE_TYPE_PNG;
		}
		else if(contentType.equals(FileMan.CONTENT_TYPE_PDF)){
			filetype=FileMan.FILE_TYPE_PDF;
		}
		else if(contentType.equals(FileMan.CONTENT_TYPE_POWERPOINT)){
			filetype=FileMan.FILE_TYPE_PPT;
		}
		else if(contentType.equals(FileMan.CONTENT_TYPE_WORD)){
			filetype=FileMan.FILE_TYPE_DOC;
		}

			

    	return filetype;
    }

	/**
	 * create file
	 * 
	 * @param pd
	 * @param file_type
	 * @param file_kind
	 * @param full_name
	 * @return IData
	 * @throws Exception
	 */
	public IData createFile(PageData pd, String file_type, String file_kind,
			String full_name) throws Exception {
		UtilDAO dao = new UtilDAO(pd, AppFactory.CENTER_CONNECTION_NAME);

		File file = FileMan.getFile(full_name);

		String file_id = dao.getSequence("SEQ_FILE_ID");
		String file_name = FileMan.getFileName(file.getName());
		String file_path = FileMan.getFilePath(file.getPath());
		String file_size = String.valueOf(file.length());

		file.renameTo(new File(file_path + "/" + file_id));

		IData data = new DataMap();
		data.put("FILE_ID", file_id);
		data.put("FILE_TYPE", file_type);
		data.put("FILE_KIND", file_kind);
		data.put("FILE_NAME", file_name);
		data.put("FILE_PATH", file_path);
		data.put("FILE_SIZE", file_size);
		createFile(pd, data);

		return data;
	}

	/**
	 * update file
	 * 
	 * @param pd
	 * @param data
	 * @throws Exception
	 */
	public void updateFile(PageData pd, IData data) throws Exception {
		UtilDAO dao = new UtilDAO(pd, AppFactory.CENTER_CONNECTION_NAME);
		dao.update("TD_M_FILE", data);
	}

	/**
	 * delete file
	 * 
	 * @param pd
	 * @param file_id
	 * @throws Exception
	 */
	public void deleteFile(PageData pd, String file_id) throws Exception {
		UtilDAO dao = new UtilDAO(pd, AppFactory.CENTER_CONNECTION_NAME);

		IData data = queryFile(pd, file_id);

		FileMan.deleteFile((String) data.get("FILE_PATH"), (String) data.get("R_FILE_NAME"));

		dao.delete("TD_M_FILE", data);
	}

	/**
	 * delete files
	 * 
	 * @param pd
	 * @param files
	 * @throws Exception
	 */
	public void deleteFiles(PageData pd, String[] files) throws Exception {
		for (int i = 0; i < files.length; i++) {
			deleteFile(pd, files[i]);
		}
	}

	/**
	 * query file
	 * 
	 * @param pd
	 * @param file_id
	 * @return IData
	 * @throws Exception
	 */
	public IData queryFile(PageData pd, String file_id) throws Exception {
		UtilDAO dao = new UtilDAO(pd, AppFactory.CENTER_CONNECTION_NAME);

		IData data = new DataMap();
		data.put("FILE_ID", file_id);
		return dao.queryByPK("TD_M_FILE", data);
	}

	/**
	 * query files
	 * 
	 * @param pd
	 * @param file_type
	 * @param pagination
	 * @return IDataset
	 * @throws Exception
	 */
	public IDataset queryFiles(PageData pd, String file_type,
			Pagination pagination) throws Exception {
		UtilDAO dao = new UtilDAO(pd, AppFactory.CENTER_CONNECTION_NAME);
		return dao.queryFiles(file_type, pagination);
	}

	/**
	 * query Static Param
	 * 
	 * @param pd
	 * @param type_id
	 * @param data_id
	 * @return
	 * @throws Exception
	 */
	public IData queryStaticParam(PageData pd, String type_id) throws Exception {
		UtilDAO dao = new UtilDAO(pd, AppFactory.CENTER_CONNECTION_NAME);
		return dao.queryStaticParam(type_id);
	}

	/**
	 * query Params By Parent For Tree
	 * 
	 * @param pd
	 * @param param
	 * @param str
	 * @return
	 * @throws Exception
	 */
	public IDataset queryStaticParamsByParentForTree(PageData pd,
			String parent_id, boolean isRoot) throws Exception {

		UtilDAO dao = new UtilDAO(pd, AppFactory.CENTER_CONNECTION_NAME);

		return dao.queryStaticParamsByParentForTree(parent_id, isRoot);

	}

	/**
	 * query Params By Parent
	 * 
	 * @param pd
	 * @param parent_id
	 * @param isRoot
	 * @return
	 * @throws Exception
	 */
	public IDataset queryStaticParamsByParent(PageData pd, String parent_id)
			throws Exception {

		UtilDAO dao = new UtilDAO(pd, AppFactory.CENTER_CONNECTION_NAME);

		return dao.queryStaticParams(parent_id);

	}

	/**
	 * 提交前台操作的数据
	 * 
	 * @param pd
	 * @param param
	 * @param dataset
	 * @throws Exception
	 */
	public int submitParams(PageData pd, IData param, IDataset dataset)
			throws Exception {
		UtilDAO dao = new UtilDAO(pd, AppFactory.CENTER_CONNECTION_NAME);

		for (int i = 0; i < dataset.size(); i++) {
			IData data = (IData) dataset.get(i);
			data.put("TYPE_ID", param.get("TYPE_ID"));
			data.put("VALID_FLAG", "1");

			String x_tag = (String) data.get("X_TAG");
			IData sparam = dao.queryByPK("TD_S_STATIC", data);
			IData sp = new DataMap();
			sp.put("TYPE_ID", data.get("TYPE_ID"));
			sp.put("DATA_NAME", data.get("DATA_NAME"));
			IData sdata = dao.queryStaticByName(sp);

			if ("0".equals(x_tag)) {
				if (sparam == null) {
					if (sdata == null) {
						dao.insert("TD_S_STATIC", data);
					} else
						return 2;
				} else {
					if (sdata == null) {
						dao.update("TD_S_STATIC", data);
					} else {
						sp.put("DATA_ID", data.get("DATA_ID"));
						IData sname = dao.queryStaticByName(sp);
						if (sname == null)
							return 2;
					}
				}
			}
			if ("1".equals(x_tag))
				dao.delete("TD_S_STATIC", data);
			if ("2".equals(x_tag))
				dao.update("TD_S_STATIC", data);
		}
		IData type = dao.queryByPK("TD_S_STATIC_TYPE",
				new String[] { "TYPE_ID" }, new String[] { (String) param
						.get("TYPE_ID") });
		IData sparam = new DataMap();
		sparam.put("SUBSYS_CODE", param.get("SUBSYS_CODE"));
		sparam.put("TYPE_DESC", param.get("TYPE_DESC"));
		IData desc = dao.queryStaticTypeByDesc(sparam);
		param.put("VALID_FLAG", "1");
		if (type == null) {
			if (desc == null) {
				dao.insert("TD_S_STATIC_TYPE", param);
			} else {
				return 1;
			}
		} else {
			IDataset pa = dao.queryStaticParams((String) param.get("TYPE_ID"));
			if (pa.size() == 0) {
				IData param2 = new DataMap();
				param2.put("TYPE_ID", param.get("TYPE_ID"));
				dao.delete("TD_S_STATIC_TYPE", param2);
			} else {
				if (desc == null) {
					dao.save("TD_S_STATIC_TYPE", param);
				} else {
					sparam.put("TYPE_ID", param.get("TYPE_ID"));
					IData sdesc = dao.queryStaticTypeByDesc(sparam);
					if (sdesc == null)
						return 1;
				}
			}
		}
		ICache cache = EHCacheManager.getInstance().getCache(
				"COM_STATIC_COLLECT");
		if (cache != null) {
			cache.remove((String) param.get("TYPE_ID"));
		}
		return 0;
	}

	/**
	 * delete params
	 * 
	 * @param pd
	 * @param param
	 * @param dataset
	 * @throws Exception
	 */
	public void deleteParams(PageData pd, IDataset dataset) throws Exception {
		UtilDAO dao = new UtilDAO(pd, AppFactory.CENTER_CONNECTION_NAME);
		String type_id = (String) ((IData) dataset.get(0)).get("TYPE_ID");

		dao.delete("TD_S_STATIC", dataset);
		IDataset daset = dao.queryStaticParams(type_id);
		if (daset.size() == 0) {
			dao.delete("TD_S_STATIC_TYPE", dataset);
		}

		ICache cache = EHCacheManager.getInstance().getCache(
				"COM_STATIC_COLLECT");
		if (cache != null) {
			cache.remove(type_id);
		}

	}

}
