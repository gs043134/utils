create table TD_M_FILE
(
  FILE_ID     NUMBER(10) not null,
  FILE_NAME   VARCHAR2(100) not null,
  R_FILE_NAME VARCHAR2(10),
  FILE_KIND   VARCHAR2(5),
  FILE_PATH   VARCHAR2(100),
  FILE_SIZE   NUMBER(12),
  CREA_STAFF  CHAR(8),
  CREA_TIME   DATE
)
;
comment on column TD_M_FILE.FILE_ID
  is ''文件标识'';
comment on column TD_M_FILE.FILE_NAME
  is ''本地文件名称'';
comment on column TD_M_FILE.R_FILE_NAME
  is ''服务器上文件名'';
comment on column TD_M_FILE.FILE_KIND
  is ''文件类型'';
comment on column TD_M_FILE.FILE_PATH
  is ''文件路径'';
comment on column TD_M_FILE.FILE_SIZE
  is ''文件大小，单位字节'';
comment on column TD_M_FILE.CREA_STAFF
  is ''创建员工'';
comment on column TD_M_FILE.CREA_TIME
  is ''创建时间'';
alter table TD_M_FILE
  add constraint PK_T_FILE primary key (FILE_ID);