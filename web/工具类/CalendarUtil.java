package com.linkage.olcom.common;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class CalendarUtil {

	/**
	 * 昨天的当前时间
	 * 
	 * @return
	 */
	public static Calendar getYesterday() {
		Calendar day = Calendar.getInstance();
		day.add(Calendar.DAY_OF_MONTH, -1);
		return day;
	}

	/**
	 * 前天的当前时间
	 * 
	 * @return
	 */
	public static Calendar getTheDayBeforeYesterday() {
		Calendar day = Calendar.getInstance();
		day.add(Calendar.DAY_OF_MONTH, -2);
		return day;
	}

	/**
	 * 本周第一天（星期一）
	 * 
	 * @return
	 */
	public static Calendar getFirstDayOfWeek() {
		Calendar c = Calendar.getInstance();
		// 默认时，每周第一天为星期日，需要更改为星期一
		c.setFirstDayOfWeek(Calendar.MONDAY);
		c.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
		return c;
	}

	/**
	 * 上周第一天（星期一）
	 * @return
	 */
	public static Calendar getFirstDayOfLastWeek(){
		Calendar c = Calendar.getInstance();
		// 默认时，每周第一天为星期日，需要更改为星期一
		c.setFirstDayOfWeek(Calendar.MONDAY);
		c.add(Calendar.DAY_OF_MONTH, -7);
		c.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
		return c;
	}
	
	/**
	 * 上上周第一天（星期一）
	 * @return
	 */
	public static Calendar getFirstDayOfLastLastWeek(){
		Calendar c = Calendar.getInstance();
		// 默认时，每周第一天为星期日，需要更改为星期一
		c.setFirstDayOfWeek(Calendar.MONDAY);
		c.add(Calendar.DAY_OF_MONTH, -14);
		c.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
		return c;
	}
	
	/**
	 * 本周最后一天（星期日）
	 * 
	 * @return
	 */
	public static Calendar getLastDayOfWeek() {
		Calendar c = Calendar.getInstance();
		// 默认时，每周第一天为星期日，需要更改为星期一
		c.setFirstDayOfWeek(Calendar.MONDAY);
		c.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
		return c;
	}
	
	/**
	 * 上周最后一天（星期日）
	 * @return
	 */
	public static Calendar getLastDayOfLastWeek() {
		Calendar c = Calendar.getInstance();
		// 默认时，每周第一天为星期日，需要更改为星期一
		c.setFirstDayOfWeek(Calendar.MONDAY);
		c.add(Calendar.DAY_OF_MONTH, -7);
		c.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
		return c;
	}
	
	/**
	 * 上上周最后一天（星期日）
	 * @return
	 */
	public static Calendar getLastDayOfLastLastWeek() {
		Calendar c = Calendar.getInstance();
		// 默认时，每周第一天为星期日，需要更改为星期一
		c.setFirstDayOfWeek(Calendar.MONDAY);
		c.add(Calendar.DAY_OF_MONTH, -14);
		c.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
		return c;
	}

	/**
	 * 当月第一天，时间为当前时间
	 * @return
	 */
	public static Calendar getFirstDayOfMonth() {
		Calendar c = Calendar.getInstance();
		c.set(Calendar.DAY_OF_MONTH, c.getActualMinimum(Calendar.DAY_OF_MONTH));
		return c;
	}
	
	/**
	 * 上月第一天，时间为当前时间
	 * @return
	 */
	public static Calendar getFirstDayOfLastMonth() {
		Calendar c = Calendar.getInstance(); 
		c.set(Calendar.DATE, 1);
		c.add(Calendar.MONTH, -1);
		return c;
	}
	
	/**
	 * 上上月第一天，时间为当前时间
	 * @return
	 */
	public static Calendar getFirstDayOfLastLastMonth() {
		Calendar c = Calendar.getInstance(); 
		c.set(Calendar.DATE, 1);
		c.add(Calendar.MONTH, -2);
		return c;
	}

	/**
	 * 当月最后一天，时间为当前时间
	 * @return
	 */
	public static Calendar getLastDayOfMonth() {
		Calendar c = Calendar.getInstance();    
	    c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));    
		return c;
	}
	
	/**
	 * 上月最后一天，时间为当前时间
	 * @return
	 */
	public static Calendar getLastDayOfLastMonth() {
		Calendar c = Calendar.getInstance();  
		c.add(Calendar.MONTH, -1);
	    c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));    
		return c;
	}
	
	/**
	 * 上上月最后一天，时间为当前时间
	 * @return
	 */
	public static Calendar getLastDayOfLastLastMonth() {
		Calendar c = Calendar.getInstance();  
		c.add(Calendar.MONTH, -2);
	    c.set(Calendar.DAY_OF_MONTH, c.getActualMaximum(Calendar.DAY_OF_MONTH));    
		return c;
	}

	/**
	 * 返回去年第一个月
	 * @return
	 */
	public static Calendar getFirstMonthOfLastYear() {
		Calendar c = Calendar.getInstance(); 
		c.set(Calendar.MONTH, 0);
		c.add(Calendar.YEAR, -1);
		return c;
	}
	
	/**
	 * 返回去年最后一个月
	 * @return
	 */
	public static Calendar getLastMonthOfLastYear() {
		Calendar c = Calendar.getInstance(); 
		c.set(Calendar.MONTH, 11);
		c.add(Calendar.YEAR, -1);
		return c;
	}
	/**
	 * 返回今年最后一个月
	 * @return
	 */
	public static Calendar getLastMonthOfThisYear() {
		Calendar c = Calendar.getInstance(); 
		c.set(Calendar.MONTH, 11);
		return c;
	}
	
	public static void main(String[] args) {
		// getFirstDayOfWeek();
		// getLastDayOfWeek();
		System.out.println(new SimpleDateFormat("yyyy-MM-dd hh:mm:ss")
				.format(CalendarUtil.getLastDayOfLastLastMonth().getTime()));
	}
	
	/**
	 * 返回两个日期之间的日期数组
	 * 以字符串形式返回
	 * 传入的起止日期是Date类型
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public static List<String> getDays(Date startDate,Date endDate){
		List<String> dates=new ArrayList<String>();
		SimpleDateFormat sdf=new SimpleDateFormat("yyyyMMdd");
		Date bdate=startDate;
		Date edate=endDate;
		while(!bdate.after(edate)){
			dates.add(sdf.format(bdate));
			bdate=getNextDate(bdate);		
		}
		return dates;
	}
	
	/**
	 * 返回两个日期之间的日期数组
	 * 以字符串形式返回
	 * 传入的起止日期是String类型
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public static List<String> getDays(String startDate,String endDate){
		SimpleDateFormat sdf=new SimpleDateFormat("yyyyMMdd");
		List<String> dates=new ArrayList<String>();
		try {
			dates=getDays(sdf.parse(startDate),sdf.parse(endDate));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return dates;
	}
	
	/**
	 * 返回两个日期之间的月份数组
	 * 以字符串形式返回
	 * 传入的起止月份是String类型
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public static List<String> getMonths(String bMonthStr,String eMonthStr){
		SimpleDateFormat sdf=new SimpleDateFormat("yyyyMM");
		List<String> months=new ArrayList<String>();
		try {
			months=getMonths(sdf.parse(bMonthStr),sdf.parse(eMonthStr));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return months;
	}
	
	/**
	 * 返回两个日期之间的月份数组
	 * 以字符串形式返回
	 * 传入的起止月份是Date类型
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public static List<String> getMonths(Date startDate,Date endDate){
		List<String> dates=new ArrayList<String>();
		SimpleDateFormat sdf=new SimpleDateFormat("yyyyMM");
		Date bdate=startDate;
		Date edate=endDate;
		while(!bdate.after(edate)){
			dates.add(sdf.format(bdate));
			bdate=getNextMonth(bdate);		
		}
		return dates;
	}
	/**
	 * 获得下一天的日期
	 * @param d
	 * @return
	 */
	public static Date getNextDate(Date d){    
		Calendar c= Calendar.getInstance();   
		c.setTime(d);
        c.add(Calendar.DAY_OF_MONTH, 1);    
       return c.getTime();
    }   
	/**
	 * 获得下一个月
	 * 格式yyyMM
	 * @param d
	 * @return
	 */
	public static Date getNextMonth(Date d){    
		Calendar c= Calendar.getInstance();   
		c.setTime(d);
        c.add(Calendar.MONTH, 1);
       return c.getTime();
    }    
}
