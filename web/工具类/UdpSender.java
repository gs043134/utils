package com.linkage.olcom.common;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

public class UdpSender {
	
	private DatagramSocket socket;
	private InetAddress target;
	private int port;
	
	public UdpSender(DatagramSocket socket, InetAddress target, int port){
		this.socket = socket;
		this.target = target;
		this.port = port;
	}
	
	public void send(String message) throws IOException{
		byte[] buf = message.getBytes("UTF-8");
		DatagramPacket dp = new DatagramPacket(buf, buf.length, target, port);
		socket.send(dp);
	}
	
	public void close(){
		this.socket.close();
	}
}
