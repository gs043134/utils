package daisen.utils.生成bean工具类;

import java.util.HashSet;

import org.apache.commons.lang.StringUtils;

/**
 * 生成bean工具类
 * ScaffoldUtil
 * @author zhaoyong
 */
public class ScaffoldUtil {
    /**
     * 去除下划线 返回首字母大写字符串
     * @param name 名称
     * @return String
     */
    public static String builderName(String name) {
        if (StringUtils.trimToNull(name) == null)
            return "";
        StringBuffer temp = new StringBuffer();
        String[] str = name.split("_");
        if (str.length == 1) {
            temp.append(str[0].substring(0, 1).toUpperCase() + str[0].substring(1));
        } else {
            for (int i=0; i<str.length; i++) {
                temp.append(str[i].substring(0, 1).toUpperCase());
                temp.append(str[i].substring(1).toLowerCase());
            }
        }
        String result = temp.toString();
        return result;
    }

    /**
     * 去除下划线 返回首字母小写字符串
     * @param name 名称
     * @return String
     */
    public static String coderName(String name) {
        if (StringUtils.trimToNull(name) == null)
            return "";
        StringBuffer temp = new StringBuffer();
        String[] str = name.split("_");
        if (str.length == 1) {
            temp.append(str[0].substring(0, 1).toUpperCase() + str[0].substring(1));
        } else {
            for (int i=0; i<str.length; i++) {
                temp.append(str[i].substring(0, 1).toUpperCase());
                temp.append(str[i].substring(1).toLowerCase());
            }
        }
        String result = temp.toString();
        result = result.substring(0, 1).toLowerCase() + result.substring(1);
        return result;
    }

    /**
     * 获得基本类型 例:varchar转换为string
     * @param type 类型
     * @return String
     */
    public static String getBaseType(String type) {
        if (StringUtils.trimToNull(type) == null)
            return "";
        type = coderName(type);
        if (type.contains("varchar")) {
            return "String";
        } else if (type.contains("decimal")) {
            return "Double";
        } else if (type.contains("int")) {
            return "Integer";
        } else {
            return builderName(type);
        }
    }

    /**
     * 获得import 例:date类型导入import java.util.Date;
     * @param array 数组方式:导入包类型
     * @return String
     */
    public static String getImportType(String[] array) {
        HashSet<String> set = new HashSet<String>();
        for (int i=0; i<array.length; i++) {
            set.add(array[i]);
        }

        StringBuffer result = new StringBuffer();
        for (String s : set) {
            String type = builderName(s);
            if (type.contains("Date")) {
                result.append("import java.util.Date;\n");
            } else if (type.contains("Timestamp")) {
                result.append("import java.sql.Timestamp;\n");
            } else if (type.contains("Blob")) {
                result.append("import java.sql.Blob;\n");
            } else if (type.contains("Clob")) {
                result.append("import java.sql.Clob;\n");
            }
        }
        return result.toString();

    }

    /**
     * 生成实体属性注释
     * @param name 实体对象名称
     * @return String
     */
    public static String createObjectComment(String name) {
        String result = "/**\n\t * " + name + "\n\t */\n";
        return result;
    }

    /**
     * 生成实体属性
     * @param name 实体对象名称
     * @param type  实体对象属性
     * @return String
     */
    public static String createObjectName(String name, String type) {
        type = getBaseType(type);
        String result = "\tprivate " + builderName(type) + " " + coderName(name) + ";\n";
        return result;
    }

    /**
     * 生成get方法
     * @param name  实体对象名称
     * @param type  实体对象属性
     * @return String
     */
    public static String createGetName(String name, String type) {
        type = getBaseType(type);
        String result = "\tpublic " + builderName(type) + " get" + builderName(name) + "() {\n\t\treturn " + coderName(name) + ";\n\t}\n";
        return result;
    }

    /**
     * 生成set方法
     * @param name  实体对象名称
     * @param type  实体对象属性
     * @return String
     */
    public static String createSetName(String name, String type) {
        type = getBaseType(type);
        String result = "\tpublic void set" + builderName(name) + "(" + builderName(type) + " " + coderName(name) +") {\n\t\tthis." + coderName(name) + " = " + coderName(name) + ";\n\t}\n";
        return result;
    }

    /**
     * 生成hibernate.hbm.xml property
     * @param name          实体对象名称
     * @param type          数据库字段类型
     * @param column        数据库字段列名称
     * @param length            数据库字段长度
     * @param isNullable    是否允许为空值: yes/no
     * @return String
     */
    public static String createXmlProperty(String name, String type, String column, String length, String isNullable) {
        name = coderName(name);
        type = coderName(getBaseType(type));
        StringBuffer result = new StringBuffer();
        if (name.equalsIgnoreCase("id")) {
            result.append("<id name=\"id\" type=\""+type+"\">\n");
            result.append("\t\t\t<column name=\"ID\" length=\""+length+"\" />\n");
            result.append("\t\t</id>\n");
        } else {
            String propertyName = "name=\""+name+"\"";
            String propertyType = "type=\""+type+"\"";
            String propertyColumn = "column=\""+column.toUpperCase()+"\"";
            String propertyLength = "";
            if (type.equalsIgnoreCase("integer")
                    || type.equalsIgnoreCase("int")
                    || type.equalsIgnoreCase("double")
                    || type.equalsIgnoreCase("float")
                    || type.equalsIgnoreCase("boolean")
                    || type.equalsIgnoreCase("timestamp")
                    || type.equalsIgnoreCase("date")) {
                propertyLength = "";
            } else {
                propertyLength = "length=\""+length+"\"";
            }
            String propertyIsNullable = "";
            /* hbm配置去掉not-null=\"true\" */
//          if (isNullable.equalsIgnoreCase("NO")) {
//              propertyIsNullable = "not-null=\"true\"";
//          }
            result.append("\t\t<property "+propertyName+" "+propertyType+" "+propertyColumn+" "+propertyLength+" "+propertyIsNullable+ "/>\n");
        }

        return result.toString();
    }

    /**
     * 生成hibernate.hbm.xml set属性的one-to-many
     * @param name      实体对象名称
     * @param clazz     实体类名称
     * @param column    数据库字段列名称
     * @param cascade   级联生命周期
     * @param inverse   反向关联(建议为true)
     * @param lazy      延迟加载(建议为false)
     * @return String
     */
    public static String createXmlOneToManySet(String name, String clazz, String column, String cascade, boolean inverse, boolean lazy) {
        name = coderName(name);
        clazz = builderName(clazz);
        StringBuffer result = new StringBuffer();
        result.append("\t\t<set name=\""+name+"\" cascade=\""+cascade+"\" inverse=\""+inverse+"\" lazy=\""+lazy+"\">\n");
        result.append("\t\t\t<key column=\""+column.toUpperCase()+"\"></key>\n");
        result.append("\t\t\t<one-to-many class=\""+clazz+"\" />\n");
        result.append("\t\t</set>\n");
        return result.toString();
    }

    /**
     * 生成hibernate.hbm.xml many-to-one
     * @param name          实体对象名称
     * @param clazz         实体类名称
     * @param column        数据库字段列名称
     * @param isNullable    是否为空(建议为true)
     * @param lazy          延迟加载(建议为false)
     * @return String
     */
    public static String createXmlManyToOne(String name, String clazz, String column, boolean isNullable, boolean lazy) {
        name = coderName(name);
        clazz = builderName(clazz);
        StringBuffer result = new StringBuffer();
        /* hbm配置去掉not-null=\""+isNullable+"\" */
        result.append("\t\t<many-to-one name=\""+name+"\" column=\""+column.toUpperCase()+"\" class=\""+clazz+"\" lazy=\""+lazy+"\" not-found=\"ignore\"></many-to-one>\n");
        return result.toString();
    }
}
