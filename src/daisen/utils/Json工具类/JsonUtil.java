package daisen.utils.Json工具类;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.hwork.annotation.Json;

/**
 * Json工具类，用户List,Map与Json的转换
 * @author heng.ai
 *
 */
public class JsonUtils {

    private static String toJson(Object obj,Class<? extends Annotation> annotationClass , boolean all) throws IllegalArgumentException, IllegalAccessException{
        if(obj == null)
            return "null";
        if(obj instanceof String)
            return "\"" + (String)obj + "\"";
        if(obj instanceof Integer)
            return ((Integer)obj).toString();
        if(obj instanceof Double)
            return ((Double)obj).toString();
        if(obj instanceof Float)
            return ((Float)obj).toString();
        if(obj instanceof Short)
            return ((Short)obj).toString();
        if(obj instanceof Long)
            return ((Long)obj).toString();
        if(obj instanceof Byte)
            return ((Byte)obj).toString();
        if(obj instanceof Character)
            return "\"" + ((Character)obj).toString() + "\"";
        if(obj instanceof Boolean)
            return ((Boolean)obj).toString();
        if(obj instanceof Date)
            return "\"" + ((Date)obj).getTime() + "\"";
        StringBuffer json = new StringBuffer();
        json.append("{");
        Class<?> clazz = obj.getClass();
        Field[] fields = clazz.getDeclaredFields();
        for(Field field : fields){
            field.setAccessible(true);
            if(all){
                json.append("\"").append(field.getName()).append("\"").append(":").append(toJson(field.get(obj), annotationClass, all)).append(",");
                continue;
            }
            if(field.isAnnotationPresent(annotationClass)){
                json.append("\"").append(field.getName()).append("\"").append(":").append(toJson(field.get(obj), annotationClass, all)).append(",");
            }
        }
        if(json.lastIndexOf(",") != -1){
            json.deleteCharAt(json.lastIndexOf(","));
        }
        json.append("}");
        return json.toString();
    }

    public static String toJson(Object obj) throws IllegalArgumentException, IllegalAccessException{
        return toJson(obj, Json.class, true);
    }

    public static String toJson(Object obj, boolean isAll) throws IllegalArgumentException, IllegalAccessException{
        return toJson(obj, Json.class, isAll);
    }

    /**
     * 将对象转换为json数据
     * annotationclass为自定义annotation，可以自己定义要列入json的字段
     * @param obj
     * @param annotationClass
     * @return
     * @throws IllegalAccessException
     * @throws IllegalArgumentException
     */
    public static String toJson(Object obj, Class<? extends Annotation> annotationClass) throws IllegalArgumentException, IllegalAccessException{
        return toJson(obj, annotationClass, false);
    }

    public static String toJson(List<?> list) throws IllegalArgumentException, IllegalAccessException{
        return toJson(list, true);
    }

    public static String toJson(List<?> list, boolean isAll) throws IllegalArgumentException, IllegalAccessException{
        StringBuffer json = new StringBuffer();
        json.append("[");
        for (Object o : list) {
            json.append(toJson(o, isAll)).append(",");
        }
        if(json.lastIndexOf(",") != -1){
            json.deleteCharAt(json.lastIndexOf(","));
        }
        json.append("]");
        return json.toString();
    }

    public static String toJson(Map<String,?> map, boolean isAll) throws IllegalArgumentException, IllegalAccessException{
        StringBuffer json = new StringBuffer();
        json.append("[");
        for (Map.Entry<String, ?> entry : map.entrySet()) {
            json.append("{\"").append(entry.getKey()).append("\"").append(":").append(toJson(entry.getValue(), isAll)).append("},");
        }
        if(json.lastIndexOf(",") != -1){
            json.deleteCharAt(json.lastIndexOf(","));
        }
        json.append("]");
        return json.toString();
    }

    public static String toJson(Map<String,?> map) throws IllegalArgumentException, IllegalAccessException{
        return toJson(map, true);
    }

    public static String toJson(Object[] objs, boolean isAll) throws IllegalArgumentException, IllegalAccessException{
        StringBuffer json = new StringBuffer();
        json.append("[");
        for (Object obj : objs) {
            json.append(toJson(obj, isAll)).append(",");
        }
        if(json.lastIndexOf(",") != -1){
            json.deleteCharAt(json.lastIndexOf(","));
        }
        json.append("]");
        return json.toString();
    }

    public static String toJson(Object[] objs) throws IllegalArgumentException, IllegalAccessException{
        return toJson(objs, true);
    }

}
