package daisen.utils;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;

public class Java屏幕截取 {
    /**
     * java屏幕截取
     * @param fileName
     * @throws Exception
     */
    public static void captureScreen(String fileName) throws Exception {

        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        Rectangle screenRectangle = new Rectangle(screenSize);
        Robot robot = new Robot();
        BufferedImage image = robot.createScreenCapture(screenRectangle);
        ImageIO.write(image, "png", new File(fileName));

    }

    public static void main(String[] args) {
        try {
            captureScreen("d:\\Test22.png");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
