package daisen.utils.摘要工具类;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import com.sdt.util.math.HexUtil;

/**
 * 摘要工具类，支持MD5和SHA系列的摘要算法
 *
 * @author caijw
 */
public class DigestUtil {

    private static MessageDigest getDigest(String algorithm) {
        try {
            return MessageDigest.getInstance(algorithm);
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    public static byte[] encodeMD5(byte[] data) {
        return getDigest("MD5").digest(data);
    }

    public static String encodeMD5Hex(byte[] data) {
        return HexUtil.byte2Hex(encodeMD5(data));
    }

    public static byte[] encodeSHA1(byte[] data) {
        return getDigest("SHA-1").digest(data);
    }

    public static String encodeSHA1Hex(byte[] data) {
        return HexUtil.byte2Hex(encodeSHA1(data));
    }

    public static byte[] encodeSHA256(byte[] data) {
        return getDigest("SHA-256").digest(data);
    }

    public static String encodeSHA256Hex(byte[] data) {
        return HexUtil.byte2Hex(encodeSHA256(data));
    }

    public static byte[] encodeSHA384(byte[] data) {
        return getDigest("SHA-384").digest(data);
    }

    public static String encodeSHA384Hex(byte[] data) {
        return HexUtil.byte2Hex(encodeSHA384(data));
    }

    public static byte[] encodeSHA512(byte[] data) {
        return getDigest("SHA-512").digest(data);
    }

    public static String encodeSHA512Hex(byte[] data) {
        return HexUtil.byte2Hex(encodeSHA512(data));
    }
}
